package integration;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.jboss.resteasy.client.ProxyFactory;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mortbay.jetty.Server;
import org.mortbay.jetty.webapp.WebAppContext;
import org.oneupfordev.resteasy.api.RESTFulExampleResource;
import org.oneupfordev.resteasy.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Miguel A. Baldi Horlle
 */
public class RESTServiceIT extends Application {

    public static final Logger LOGGER = LoggerFactory.getLogger(RESTServiceIT.class);

    private static final int HTTP_PORT = 8888;

    private static final String HTTP_HOST = "127.0.0.1"; //$NON-NLS-1$

    private static final String APP_CONTEXT = ""; //$NON-NLS-1$

    private static final String HTTP_SERVICE_ENDPOINT = "http://" + HTTP_HOST + ":" + HTTP_PORT + APP_CONTEXT; //$NON-NLS-1$ //$NON-NLS-2$

    private static Server server;

    @BeforeClass
    public static void startJetty() throws Exception {
	LOGGER.info("Inicializando servidor HTTP para testes de integracao dos servicos REST na porta {}...", String.valueOf(HTTP_PORT)); //$NON-NLS-1$
	server = new Server(HTTP_PORT);

	WebAppContext context = new WebAppContext();
	context.setDescriptor("./src/main/webapp/WEB-INF/web.xml");
	context.setResourceBase("./src/main/webapp");
	context.setContextPath("/");
	context.setParentLoaderPriority(true);

	server.setHandler(context);

	server.start();
    }

    @Test
    public void testAuthenticationOnUserResource() {
	RESTFulExampleResource client = ProxyFactory.create(RESTFulExampleResource.class, HTTP_SERVICE_ENDPOINT);
	List<Long> ids = new ArrayList<Long>();
	ids.add(Long.valueOf(21));
	ids.add(Long.valueOf(33));
	Set<Person> response = client.getPeopleByIds(ids);
	Assert.assertNotNull(response);

    }

    @Test
    @Ignore
    public void testGetPerson() {
	RESTFulExampleResource client = ProxyFactory.create(RESTFulExampleResource.class, HTTP_SERVICE_ENDPOINT);
	List<Long> ids = new ArrayList<Long>();
	ids.add(Long.valueOf(21));
	Person response = client.getPersonById(31L);
	Assert.assertNotNull(response);

    }

    @AfterClass
    public static void tearDown() throws Exception {
	server.stop();
    }

    @Override
    public Set<Class<?>> getClasses() {
	HashSet<Class<?>> tmp = new HashSet<Class<?>>();
	tmp.add(RESTFulExampleResource.class);
	return tmp;
    }

}
