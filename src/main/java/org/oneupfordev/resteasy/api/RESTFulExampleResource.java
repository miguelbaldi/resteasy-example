package org.oneupfordev.resteasy.api;

import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.oneupfordev.resteasy.domain.Person;

@Path("/example")
public interface RESTFulExampleResource {
    @GET
    @Path("/people")
    @Produces(MediaType.APPLICATION_JSON)
    Set<Person> getPeopleByIds(@QueryParam("id") List<Long> ids);

    @GET
    @Path("person")
    @Produces(MediaType.APPLICATION_JSON)
    Person getPersonById(@QueryParam("id") Long id);
}
