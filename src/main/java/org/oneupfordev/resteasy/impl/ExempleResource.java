package org.oneupfordev.resteasy.impl;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.oneupfordev.resteasy.api.RESTFulExampleResource;
import org.oneupfordev.resteasy.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExempleResource implements RESTFulExampleResource {

    Logger logger = LoggerFactory.getLogger(ExempleResource.class);

    @Override
    public Set<Person> getPeopleByIds(List<Long> ids) {
	logger.info("Finding people with identifiers={}", ids);
	Set<Person> people = new HashSet<Person>();
	for (Long id : ids) {
	    Person person = new Person();
	    person.setId(id);
	    person.setAge(Integer.valueOf(18));
	    person.setName("John Doe " + id);
	    people.add(person);
	}
	logger.info("{} people found.", people.size());
	return people;
    }

    @Override
    public Person getPersonById(Long id) {
	Person person = new Person();
	person.setId(id);
	person.setAge(Integer.valueOf(16));
	person.setName("Jane Doe " + id);
	return person;
    }

}
