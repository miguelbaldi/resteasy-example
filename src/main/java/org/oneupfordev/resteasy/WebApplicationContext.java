package org.oneupfordev.resteasy;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackageClasses = { org.oneupfordev.resteasy.WebApplicationContext.class })
public class WebApplicationContext {

}
